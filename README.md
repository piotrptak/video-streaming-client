# Video Streaming Demo Client

This project contains frontend of the video streaming platform by Meshmark (demo purposes).

## Getting started

You can just use 

```
npm start
```

That will start the development server on default port (3000).

## Development

Development is split into two main branches.

### master

Contains stable, reliable and tested version. Also for production build.
Merge stage -> master

### stage

Merge feature_branches -> stage

## Deployment

Push to stage for staging build.
Push to master for production build.
