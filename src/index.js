import React from 'react';
import ReactDOM from 'react-dom';
import './styles/global.scss';
import HomePage from './pages/HomePage';

ReactDOM.render(<HomePage />, document.getElementById('root'));
