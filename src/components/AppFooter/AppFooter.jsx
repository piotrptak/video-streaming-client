import React from 'react'
import './AppFooter.scss'

export default function AppFooter() {
  return (
    <footer className="app-footer">
      <h4>&copy; Meshmark, 2019</h4>
    </footer>
  )
}
