import React from 'react';
import './AppHeader.scss';

export default function AppHeader() {
  return (
    <header className="app-header">
      <h1>
        Meshmark Video Streaming
      </h1>
    </header>
  )
}
