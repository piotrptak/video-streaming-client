import React from 'react';
import Layout from '../components/Layout/Layout';
import AppHeader from '../components/AppHeader/AppHeader';
import AppFooter from '../components/AppFooter/AppFooter';

function HomePage() {
  return (
    <Layout className="App">
      <AppHeader />
      <main>
        <video controls>
          <source src='rtmp://s1d95sw07k6xqd.cloudfront.net/cfx/st/syme-home.mp4' type='rtmp/mp4'/>
        </video>
      </main>
      <AppFooter />
    </Layout>
  );
}

export default HomePage;
